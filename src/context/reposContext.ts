import IRepo from '../intercaces/Repo.interface';
import {createContext, useContext} from 'react';

type ReposContextType = {
    page: number,
    params: any,
    message: string,
    repos: IRepo[], 
    error: boolean, 
    loading: boolean, 
    setRepo: Function,
    setPage: Function, 
    setParams: Function,
    hasNextPage: boolean,
    handleParamchange: any,
}

export const ReposContext = createContext<ReposContextType>({
    page: 1,
    params: {},
    repos: [], 
    message: '',
    error: false, 
    loading: false,
    hasNextPage: false,
    setRepo: () => console.log('hola'),
    setPage: () => console.log('hola'), 
    setParams: () => console.log('hola'),
    handleParamchange: () => console.log('hola')
});

export const useRepos = () => useContext(ReposContext);