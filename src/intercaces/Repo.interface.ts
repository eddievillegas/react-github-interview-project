export default interface Repo {
    id: number,
    name: string,
    language: string,
    favorite: boolean,
    created_at: Date,
    description: string,
};