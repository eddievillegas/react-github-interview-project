import Repos from '../models/Repos.model';
import IRepo from '../intercaces/Repo.interface';

let repos: Array<IRepo> = [];

beforeAll( async () => {
    try {
        const [req1, req2] = await Repos.find({page:1, params: {name:'EddieVillegas', type:'user'}}); 
        repos = req1.data.concat(req2.data.items);
    } catch (error) {
        console.error(error);
    }
});

describe('Testing Repos Model', () => {
    
    test('Get repositories', async () => {
        expect(repos.length).not.toBe(0);
    });

    test('A repo has an id property', async () => {
        const [repo] = repos;
        expect(repo).toHaveProperty('id');
    });
    
    test('A repo has a name property', () => {
        const [repo] = repos;
        expect(repo).toHaveProperty('name');
    });
    
    test('A repo has a language property', () => {
        const [repo] = repos;
        expect(repo).toHaveProperty('language');
    });

    test('A repo has a created_at property', () => {
        const [repo] = repos;
        expect(repo).toHaveProperty('created_at');
    });

    test('A repo has a description property', () => {
        const [repo] = repos;
        expect(repo).toHaveProperty('description');
    });

});