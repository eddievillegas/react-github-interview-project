import React, {useState} from 'react';
import useRepos from './hooks/useRepos';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import IRepo from './intercaces/Repo.interface';
import {ReposContext} from './context/reposContext';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {
  Repos, 
  Header, 
  Loader, 
  Message,
  Wrapper,
  Bookmarks, 
  SearchForm, 
  ReposPagination, 
} from './components/index';

const App = ()  => {
  
  const [page, setPage] = useState(1);
  const [params, setParams] = useState({name: '', type: 'user'});
  const setRepo = (repo: IRepo) => repo.favorite = !repo.favorite;
  const {repos, loading, error, hasNextPage, message} = useRepos(params, page);
  const handleParamchange = (event: any) => {
      const {name, value} = event.target;
      setPage(1);
      setParams(prevParams => ({...prevParams, [name]: value}));
  };

  return(
    <ReposContext.Provider value={{ 
        page,
        repos,
        error,
        params,
        setRepo,
        loading,
        setPage,
        message,
        setParams,
        hasNextPage,
        handleParamchange,
      }}>
      <Router>
        <Container className="my-4">
          <Header/>
          <SearchForm/>
          <Wrapper>
            <ReposPagination/>
            {loading && <Loader/>}
            {message !== '' && <Message/>}
          </Wrapper>
          <Switch>
            <Route
              exact
              path="/" 
              component={Repos}
            />
            <Route 
              exact 
              path="/bookmarks"
              component={Bookmarks}
            />
          </Switch>
        </Container>
      </Router>
    </ReposContext.Provider>
  );
}

export default App;