import axios from 'axios';

interface IRepo {
    params: {name: string, type: string},
    page: number,
}

class Repos {

    static find({page, params}: IRepo) {
        const url1 = `https://api.github.com/users/${params.name}/repos?page=${page}&per_page=5`;    
        const url2 = `https://api.github.com/search/repositories?pages=${page}&per_page=5&q=${params.name}`;    
        const request1 = axios.get(url1);
        const request2 = axios.get(url2);
        return axios.all([request1, request2]);
    }

}

export default Repos;