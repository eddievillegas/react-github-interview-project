import IRepo from '../intercaces/Repo.interface';

class Repo {

    private id: number;
    private name: string;
    private language: string;
    private created_at: Date;
    private _favorite: boolean = false;
    private description: string;

    constructor({id, name, language, favorite = false, created_at, description}: IRepo){
        this.id = id;
        this.name = name;
        this.language = language;
        this.favorite = favorite;
        this.created_at = created_at;
        this.description = description;
    }

    static create(data: IRepo){
        return new Repo(data);
    }

    get favorite(): boolean {
        return this._favorite;
    }

    set favorite(value: boolean) {
        this._favorite = value;
    }

}

export default Repo;