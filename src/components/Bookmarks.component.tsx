import React, {Fragment} from 'react';
import Repo from '../components/Repo.component';
import IRepo from '../intercaces/Repo.interface';
import { useRepos } from '../context/reposContext';

const Bookmarks = () => {
    const {repos} = useRepos();
    const likesRepos = repos.filter(repo => repo.favorite);
    return (
        <Fragment>
            { likesRepos.map((repo: IRepo) => <Repo repo={repo} key={repo.id}/>) }
        </Fragment>
    );
}

export default Bookmarks;