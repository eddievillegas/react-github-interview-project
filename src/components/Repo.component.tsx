import React, {useState} from 'react';
import IRepo from '../intercaces/Repo.interface';
import {Card, Badge, Form} from  'react-bootstrap';
import { useRepos } from '../context/reposContext';

type Props = {
    repo: IRepo,
    key: number,
};

const Repo: React.FC<Props> = ({repo}) => {
    const {setRepo} = useRepos();
    const [favorite, setFavorite] = useState(repo.favorite);
    const created_at = new Date(repo.created_at).toLocaleDateString();
    const handleChange = () => {
        setFavorite((prevSate) => !prevSate);
        setRepo(repo);
    };
    return(
        <Card>
            <Card.Body>
                <div className="d-flex justify-content-between">
                    <div>
                        <Card.Title>{repo.name}</Card.Title>
                        <Card.Subtitle className="text-muted mb-2">{repo.description}</Card.Subtitle>
                        <Badge variant="secondary" className="mr-2">{repo.language}</Badge>
                        <Badge variant="secondary" className="mr-2">{created_at}</Badge>
                        <Form.Check 
                            name="like" 
                            label="Like" 
                            type="checkbox" 
                            checked={favorite}
                            onChange={handleChange} 
                        />
                    </div>
                </div>
            </Card.Body>
        </Card>
    );
};

export default Repo;