import React from 'react';
import {useRepos} from '../context/reposContext'; 

const Message: React.FC = () => {
    const {message} = useRepos();
    return(<h1 className='mb-4'>{message}</h1>);
};

export default Message;