import React, {Fragment} from 'react';
import {Pagination} from 'react-bootstrap';
import {useRepos} from '../context/reposContext';

const ReposPagination = () => {
    const {repos, page, setPage, hasNextPage} = useRepos();
    const nextPage = page + 1;
    const prevPage = page - 1;
    const adjustPage = (amount: number) => setPage((prevPage: number) => prevPage + amount);
    return (
        <Fragment>
            {repos.length > 0 &&
            <Pagination>
                {page !== 1 && <Pagination.Prev onClick={() => adjustPage(-1)} />}
                {page !== 1 && <Pagination.Item onClick={() => setPage(1)}>1</Pagination.Item>}
                {page > 2 && <Pagination.Ellipsis/>}
                {page > 2 && <Pagination.Item onClick={() => adjustPage(-1)}>{prevPage}</Pagination.Item>}
                <Pagination.Item active>{page}</Pagination.Item>
                {hasNextPage && <Pagination.Item onClick={() => adjustPage(1)}>{nextPage}</Pagination.Item>}
                {hasNextPage && <Pagination.Next onClick={() => adjustPage(1)}/>}
            </Pagination>}
        </Fragment>
    );
}

export default ReposPagination;