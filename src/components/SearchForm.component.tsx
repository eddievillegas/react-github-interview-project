import React from 'react';
import {Form, Col} from 'react-bootstrap';
import {useRepos} from '../context/reposContext';

const SearchForm: React.FC = () => {
    
    const {handleParamchange} = useRepos();
    
    return(
        <Form autoComplete="off">
            <Form.Row>
                <Form.Group as={Col}>
                    <Form.Control
                        type="text" 
                        name="name" 
                        onChange={handleParamchange}
                        placeholder="search by name..." 
                    />
                </Form.Group>
            </Form.Row>
        </Form>
    );
}

export default SearchForm;