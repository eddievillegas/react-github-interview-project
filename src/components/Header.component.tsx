import React from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';

const Header = () => (
    <Navbar bg="ligth" expand="lg">
        <Navbar.Brand>Github Repositories</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link>
                    <Link to={'/'}>Repositories</Link>
                </Nav.Link>
                <Nav.Link>
                    <Link to={'/bookmarks'}>Bookmarks</Link>
                </Nav.Link>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
)

export default Header;