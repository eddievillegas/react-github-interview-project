import {Repo} from './index';
import React, {Fragment} from 'react';
import IRepo from '../intercaces/Repo.interface';
import {useRepos} from '../context/reposContext';

const Repos: React.FC = () => {
    const {repos} = useRepos();
    return (
        <Fragment>
            {repos.map( (repo: IRepo) => <Repo key={repo.id} repo={repo}/>) }
        </Fragment>
    );
}

export default Repos;