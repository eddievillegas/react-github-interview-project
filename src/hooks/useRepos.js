import {debounce} from 'lodash';
import Repo from '../models/Repo.model';
import Repos from '../models/Repos.model';
import {useEffect, useReducer} from 'react';

const ACTIONS = {
    ERROR: 'error',
    MESSAGE: 'message',
    GET_DATA: 'get-data',
    MAKE_REQUEST: 'make-request',
    UPDATE_HAS_NEXT_PAGE: 'update-has-next-page',
}

const reducer = (state, action) => {
    switch(action.type) {
        case ACTIONS.MAKE_REQUEST:
            return { loading: true, repos: [], message: ''};
        case ACTIONS.GET_DATA:
            return { ...state, loading: false, repos: action.payload.repos, message: ''};
        case ACTIONS.ERROR:
            return { ...state, loading: false, error: action.payload.error, repos: [], message: 'Error, Try Again.'};
        case ACTIONS.UPDATE_HAS_NEXT_PAGE:
            return {...state, hasNextPage: action.payload.hasNextPage};
        case ACTIONS.MESSAGE:
            return {...state, loading: false, respos: [], message: action.payload.message}
        default:
            return state;
    }
}

const useFetchRepos = (params, page) => {
    const timeout = 3000;
    const [state, dispatch] = useReducer(reducer, {repos: [], loading: false})
    useEffect(() => {
        const getRepos = async () => {
            let message = "";
            dispatch({type: ACTIONS.MAKE_REQUEST});
            try {
                const [req1, req2] = await Repos.find({params, page});
                const repos = req1.data.concat(req2.data.items);
                if(repos.length)
                    dispatch({type: ACTIONS.GET_DATA, payload: {repos: repos.map(Repo.create)}});
                else
                    message = "There aren't repos"
                dispatch({type: ACTIONS.UPDATE_HAS_NEXT_PAGE, payload: {hasNextPage: repos.length !== 0}});
                dispatch({type: ACTIONS.MESSAGE, payload: {message}});
            } catch (error) {
                dispatch({type: ACTIONS.ERROR, payload: {error}});
            }
        };
        const debounceGetRepos = debounce(() => getRepos(), timeout);
        if(params.name) debounceGetRepos();
    }, [params, page]);
    return state;
}

export default useFetchRepos;